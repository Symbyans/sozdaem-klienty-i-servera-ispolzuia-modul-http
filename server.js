//НЕ ПАРСИТ POST запросы с json в формате application/x-www-form-urlencoded
//-----------------------------------------------------------
const http        = require('http');
const port  = process.env.PORT || 3000;
let request       = require('request');
//-----------------------------------------------------------

function handler(req, res){
    res.writeHead(200);
    let data;
    //-------------------------
    req.on('data',  function(chunk) { data=chunk ; 
    //-------------------------
    req.on('end', function() {
      request({
        method:       'POST',
        url:          'http://netology.tomilomark.ru/api/v1/hash',
        headers:{ 'Content-Type':   'application/x-www-form-urlencoded'},
      	forms: data}, 

        function (error, response, body) {
          if (error) throw new Error(error);

          let parsed_full_name = JSON.parse(data);
          let parsed_cash = JSON.parse(body);
          let client_callback = {
          	"firstName": parsed_full_name.firstName,
          	"lastName": parsed_full_name.lastName,
          	"secretKey":  parsed_cash.hash
          };

          res.end(JSON.stringify(client_callback));		}
        );
    });
})
};
//-----------------------------------------------------------



const server = http.createServer();
server
    .listen(port)
    .on('error', err => console.log(err))
    .on('request', handler)
    .on('listening', ()=>{
        console.log('Start HTTP on port %d', port);
    });